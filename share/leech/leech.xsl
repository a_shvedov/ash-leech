<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>

    <!-- this should match <enclosure url="..." -->
    <xsl:template match="item[enclosure]">
<xsl:value-of select="title"/><xsl:text> </xsl:text><xsl:value-of select="enclosure/@url"/> "<xsl:value-of select="pubDate"/>"<xsl:text><!-- line break -->
</xsl:text>
    </xsl:template>

    <!-- this should match the rest of the items with usual <link> record
    however, items with appropriate <enclosure> shouldn't match
    since they are handled with other template -->
    <xsl:template match="item[link][not(enclosure)]">
<xsl:value-of select="title"/><xsl:text> </xsl:text><xsl:value-of select="link"/> "<xsl:value-of select="pubDate"/>"<xsl:text><!-- line break -->
</xsl:text>
    </xsl:template>

    <!-- records that doesn't match any template should produce nothing
    -->
    <xsl:template match="item">
    </xsl:template>

    <!-- main()
    -->
    <xsl:template match="/">
        <xsl:apply-templates select="rss/channel/item" />
    </xsl:template>

</xsl:stylesheet>
