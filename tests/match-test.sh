#!/bin/sh

[ ! -z "$LEECHT" ] && set "$LEECHT"

. $(dirname $0)/assert.sh

HERE=$(cd "$(dirname "$0")" && pwd)  # current dir
TOOL="$HERE/../sbin/leech-match-test"

export CONFIG_DIR="$HERE/conf"

$TOOL '竹 title' 'processed.xml' >/dev/null
MATCH_RESULT=$?

$TOOL '竹 title.avi' 'processed.xml' >/dev/null
MISMATCH_RESULT=$?

assert "$MATCH_RESULT -eq 0" # matching records should return 0
assert "$MISMATCH_RESULT -ne 0" # .avi is in reverse-downloads

# tests without feed URL
#
$TOOL 'title.mkv' >/dev/null
MATCH_RESULT=$?

$TOOL 'crap.wmv' >/dev/null
MISMATCH_RESULT=$?

assert "$MATCH_RESULT -eq 0" # matching records should return 0
assert "$MISMATCH_RESULT -ne 0" # .avi is in reverse-downloads
