#!/bin/sh

[ ! -z "$LEECHT" ] && set "$LEECHT"

. $(dirname $0)/assert.sh

HERE=$(cd "$(dirname "$0")" && pwd)  # current dir
TOOL="$HERE/../sbin/leech-wild-magic"

export DOWNLOADS="$HERE/conf/downloads"
export REVERSE_DOWNLOADS="$HERE/conf/reverse-downloads"
export WILD_DOWNLOADS="$HERE/conf/wild-downloads"

assert "$(echo processed.xml 竹 title | $TOOL | wc -l) -eq 1"
assert "$(echo processed.xml 竹 title.avi | $TOOL | wc -l) -eq 0" # .avi is in reverse-downloads
